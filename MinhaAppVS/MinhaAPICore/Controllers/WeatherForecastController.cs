﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinhaAPICore.Controllers
{
    [Route("[controller]")]
    public class WeatherForecastController : MainController
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        //GET api/values/obter-por-id/5
        [HttpGet("Obter-por-id/{id:int}")]
        public ActionResult<string> Get(int id)
        {
            return id.ToString();
        }

        [HttpGet("ObterTodos")]
        public ActionResult<IEnumerable<string>> ObterTodos()
        {
            var valores = new string[] { "value1", "value2" };

            if (valores.Length < 1)
                return BadRequest();

            return Ok(valores);
        }

        [HttpGet("ObterResultado")]
        public ActionResult<IEnumerable<string>> ObterResultado()
        {
            var valores = new string[] { "value1", "value2" };

            if (valores.Length < 1)
                return CustomResponse();

            return CustomResponse(valores);
        }

        [HttpGet("ObterValores")]
        public IEnumerable<string> ObterValores()
        {
            var valores = new string[] { "value1", "value2" };

            if (valores.Length < 1)
                return null;

            return valores;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Product), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public ActionResult Post(Product product)
        { 
            if(product.Id == 0) return BadRequest();

            // add no banco
            //return Ok(product);
            return CreatedAtAction(nameof(Post), product);
        }

        [HttpPost("PostConventions")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public ActionResult PostTeste(Product product)
        {
            if (product.Id == 0) return BadRequest();

            // add no banco
            //return Ok(product);
            return CreatedAtAction(nameof(Post), product);
        }
    }

    [ApiController]
    public abstract class MainController : ControllerBase
    {
        protected ActionResult CustomResponse(object result = null)
        {
            if(OperacaoValida())
            {
                return Ok(new
                {
                    sucess = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                sucess = false,
                errors = ObterErros()
            });

        }

        public bool OperacaoValida()
        {
            return true;
        }

        protected string ObterErros()
        {
            return ""; 
        }
    }

    public class Product
    {
        public int Id {  get; set; }
        public string Name {  get; set; }

    }


        
}
